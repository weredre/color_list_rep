package com.example.color_list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.color_list.model.ColorsRepo
import kotlinx.coroutines.launch

class ColorsViewModel : ViewModel() {
    private val repo = ColorsRepo

    private val _color = MutableLiveData<List<Int>>()
    val colors : LiveData<List<Int>> get() = _color

    fun getColors(repeater: Int) {
        viewModelScope.launch {
            val colorsList = repo.getColors(repeater)
            _color.value = colorsList
        }
    }
}