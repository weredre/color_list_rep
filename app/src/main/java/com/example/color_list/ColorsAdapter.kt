package com.example.color_list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.color_list.databinding.ItemColorsBinding

class ColorsAdapter: RecyclerView.Adapter<ColorsAdapter.ColorsViewHolder>() {
    private var colors = mutableListOf<Int>()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ColorsViewHolder {
        val binding = ItemColorsBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
      return ColorsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorsViewHolder, position: Int) {
        if (colors.isNotEmpty()) {
            val color = colors[position]
            holder.loadColor(color)
        }
    }

    override fun getItemCount(): Int {
        return colors.size
    }
    @SuppressLint("NotifyDataSetChanged")
    fun addColors(colors: List<Int>){
        this.colors = colors.toMutableList()
        notifyDataSetChanged()
    }

    class ColorsViewHolder (
            private val binding: ItemColorsBinding):RecyclerView.ViewHolder(binding.root){
                fun loadColor(color: Int){
                    binding.ivPhoto.setBackgroundColor(color)
                    binding.ivPhoto.setOnClickListener {
//                        it.findNavController().navigate(ColorsFragmentDirections.actionColorFragmentToNumbersFragment(color.toString().drop(1)))
                    }
                }
            }
}