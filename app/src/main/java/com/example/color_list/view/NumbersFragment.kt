package com.example.color_list.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.color_list.NumbersAdapter
import com.example.color_list.databinding.FragmentNumbersBinding


class NumbersFragment : Fragment() {

    private var _binding: FragmentNumbersBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNumbersBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val passedColor = arguments?.getString("number")
        val splitNumber = passedColor?.split("")?.toList()

        binding.rvLetterList.layoutManager = LinearLayoutManager(this.context)
        binding.rvLetterList.adapter = NumbersAdapter().apply {
            splitNumber?.let { addNumbers(it) }
        }
    }
}