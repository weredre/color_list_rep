package com.example.color_list.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.color_list.viewmodel.ColorsViewModel
import com.example.color_list.databinding.FragmentColorsBinding
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.color_list.ColorsAdapter


class ColorsFragment:Fragment() {
    var newColors: MutableList<Int> = mutableListOf<Int>()

    private var _binding: FragmentColorsBinding? = null
    private val binding get() = _binding!!
    private val colorsViewModel by viewModels<ColorsViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorsBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvList.layoutManager = GridLayoutManager(this.context, 3)
        binding.rvList.adapter = ColorsAdapter()
        binding.button.setOnClickListener {val repeater = binding.etColors.text.toString().toInt()
            colorsViewModel.getColors(repeater)
        }
        colorsViewModel.colors.observe(viewLifecycleOwner) { list: List<Int> ->
            binding.rvList.adapter.let { adapter: RecyclerView.Adapter<*>? ->
                if (adapter is ColorsAdapter){
                     adapter.addColors(list)

                }
            }
        }

        binding.rvList.rootView.setOnClickListener {
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
        }
    }
}

