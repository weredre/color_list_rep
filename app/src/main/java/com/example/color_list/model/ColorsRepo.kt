package com.example.color_list.model

import com.example.color_list.model.remote.ColorsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.color_list.random.randomColor


object ColorsRepo {
    private val ColorApi = object : ColorsApi {
        override suspend fun getColors(repeater: Int) : List<Int> {
            var newColors: MutableList<Int> = mutableListOf()
            repeat(repeater) {
                newColors.add(randomColor)
            }
            return newColors
        }
    }

    suspend fun getColors(repeater: Int): List<Int> = withContext(Dispatchers.IO)
    {
        ColorApi.getColors(repeater)
    }
}