package com.example.color_list.model.remote

interface ColorsApi {
    suspend fun getColors(repeater: Int) : List<Int>
}