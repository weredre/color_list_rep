package com.example.color_list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.color_list.databinding.ActivityMainBinding
import kotlin.math.log

class MainActivity : AppCompatActivity() {
//    private val binding by lazy {
//        ActivityMainBinding.inflate(layoutInflater)
//    }
//    private var ints : List<Int>? = null
////    private val colors get()= ints?.map { it.toString() }?.toList().orEmpty()
////    val colors : List<Color> = ints.map { it: Int ->
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////            Color.valueOf(it)
////        } else {;
////            val hexColor = String.format("#%06X", 0xFFFFFF and it)
////            Color.parseColor(hexColor)
////        }
////    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(binding.root)
//        ints = resources.getIntArray(R.array.androidcolors).toList().orEmpty()
//        binding.rvList.layoutManager = LinearLayoutManager(this)
//        binding.rvList.adapter = ColorsAdapter().apply {addColors(ints!!) }
//    }
private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}